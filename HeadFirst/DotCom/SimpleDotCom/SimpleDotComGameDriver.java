public class SimpleDotComGameDriver
{
    public static void main(String[] args)
    {
        CliReader reader = new CliReader();
        SimpleDotComGame game = new SimpleDotComGame();

        while (!game.over())
        {
            System.out.println(
                game.makeMove(reader.getUserInput("Enter a number: "))
            );
        }
        System.out.println(game.displayResults());
    }
}

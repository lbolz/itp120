public class PopSong
{
    public static void main (String[] args)
    {
        int popNum = 99;
        String word = "cans";

        while (popNum > 0)
        {
            if (popNum == 1)
            {
                word = "can"; // singular, as in ONE bottle.
            }
            
            System.out.println(popNum + " " + word + " of pop on the wall");
            System.out.println(popNum + " " + word + " of pop.");
            System.out.println("Take one down.");
            System.out.println("Pass it around.");
            popNum = popNum - 1;

            if (popNum > 0)
            {
                System.out.println(popNum + " " + word + " of pop on the wall");
            }

            else
            {
                System.out.println("No more cans of pop on the wall");
            } // end else
        } // end while loop
    } // end main method
} // end class

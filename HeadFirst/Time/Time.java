public class Time
{
    private int myHour, myMinute, mySecond;

    public Time()
    {
        myHour = 0;
        myMinute = 0;
        mySecond = 0;
    }

    public Time(int hour, int minute, int second)
    {
        myHour = hour;
        myMinute = minute;
        mySecond = second;
    }

    public String toString()
    {
        return formatTime(myHour, myMinute, mySecond);
    }

    public void setHour(int hour)
    {
        myHour = hour;
    }

    public void setMinute(int minute)
    {
        myMinute = minute;
    }

    public void setSecond(int second)
    {
        mySecond = second;
    }

    public int getHour()
    {
        return myHour;
    }

    public int getMinute()
    {
        return myMinute;
    }

    public int getSecond()
    {
        return mySecond;
    }

    public Time addTime(Time t1)
    {
        int newHour, newMinute, newSecond;
        newHour = myHour;
        newMinute = myMinute;
        newSecond = mySecond;

        if ((newSecond + t1.getSecond()) >= 60)
            newMinute += 1;

        if ((newMinute + t1.getMinute()) >= 60)
            newHour += 1;

        newHour = (newHour + t1.getHour()) % 60;
        newMinute = (newMinute + t1.getMinute()) % 60;
        newSecond = (newSecond + t1.getSecond()) % 60;

        Time result = new Time(newHour, newMinute, newSecond);
        return result;
    }

    public String formatTime(int hour, int minute, int second)
    {
        String formatMinute, formatSecond, output;
        formatMinute = Integer.toString(minute);
        formatSecond = Integer.toString(second);
        if (minute < 10)
            formatMinute = "0" + minute;
        if (second < 10)
            formatSecond = "0" + second;
        output = hour + ":" + formatMinute + ":" + formatSecond;
        return output;
    }
}

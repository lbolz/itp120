class TimeTester
{
    public static void main(String[] args)
    {
        Time time1 = new Time();
        System.out.println(time1);
        time1.setHour(3);
        time1.setMinute(45);
        time1.setSecond(55);
        System.out.println(time1);

        Time time2 = new Time(1, 20, 11);
        System.out.println(time2);
        System.out.println(time2.getHour() + ":" + time2.getMinute() +
                            ":" + time2.getSecond());

        Time time3 = new Time();
        time3 = time1.addTime(time2);
        System.out.println(time3);
    }
}

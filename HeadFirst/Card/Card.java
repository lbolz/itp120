class Card
{
    /*
    Notes for myself:
    - final method: you cannot override the method
        - it also runs before the rest of the code, so it's a good place to
        initialize static variables
    - final variable: you cannot change the value of the variable

    - public: any code can access this type of variable
    - private: only code from this class can access these variables

    - static variable: a variable that is created at the class level and is
    shared throughout the class
    - static method: a method that belongs to a class, not an object
        - can only access static data and static methods
        - often used as utility methods (such as main())

    - the "this.__" (this class) is a keyword. it is a reference to the current
        object that is being changed
    */
    public static final String[] suits =
    {
        "Clubs", "Diamonds", "Hearts", "Spades"
    };

    public static final String[] ranks =
    {
        "narf", "Ace", "Two", "Three", "Four", "Five", "Six", "Seven",
        "Eight", "Nine", "Ten", "Jack", "Queen", "King"
    };

    private int suit;
    private int rank;

    public Card(int suit, int rank)
    {
        this.suit = suit;
        this.rank = rank;
    }

    public String toString()
    {
        return Card.ranks[rank] + " of "  + Card.suits[suit];
    }
}

import java.util.ArrayList;

public class SimpleDotComGame
{
    private ArrayList<Integer> locations;
    private int numHits = 0;
    private int numGuess = 0;

    public SimpleDotComGame()
    {
        int randomNum = (int) (Math.random() * 5);
        this.locations.add(randomNum);
        this.locations.add(randomNum+1);
        this.locations.add(randomNum+2);
    }

    public boolean over()
    {
        return this.numHits >= this.locations.size();
    }

    public String displayResults()
    {
        String out = "You took " + this.numGuess + " guesses.";
        return out;
    }

    public String checkYourself(String guess)
    {
        int userGuess = Integer.parseInt(guess);
        int locIndex = 0;

        this.numGuess++;

        for (int location: this.locations)
        {
            if (this.locations.contains(location))
            {
                locIndex++;
                this.locations.remove(locIndex);
                this.numHits++;
                if (this.numHits >= this.locations.size())
                    return "kill";
                return "hit";
            }
        }
        return "miss";
    }

}

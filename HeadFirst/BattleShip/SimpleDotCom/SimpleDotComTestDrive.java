public class SimpleDotComTestDrive
{
    public static void main(String[] args)
    {
        //test that kill works
        SimpleDotCom dot = new SimpleDotCom();
        String userGuess;
        String result;

        int[] locations = {3, 4, 5};
        dot.setLocationCells(locations);

        userGuess = "3";
        result = dot.checkYourself(userGuess);
        assert result == "hit" : "Expected hit, got " + result;
        System.out.println(result);

        userGuess = "4";
        result = dot.checkYourself(userGuess);
        assert result == "hit" : "Expected hit, got " + result;
        System.out.println(result);

        userGuess = "6";
        result = dot.checkYourself(userGuess);
        System.out.println(result);


        userGuess = "5";
        result = dot.checkYourself(userGuess);
        assert result == "kill" : "Expected kill, got " + result;
        System.out.println(result);
    }
}

import java.util.Scanner;

class Chatter
{
 public static String getRandomResponse()
  {
    String[] responses = { "Interesting. Tell me more.",
                 "Hmmm.",
                 "Do you really think so?",
                 "You don't say.",
                 "I see.",
                 "Many people say that." };
    int randomIndex = (int)(Math.random() * responses.length);

    return responses[randomIndex];
  }

  public static String getResponse( String statement )
  {
    String response = "";
    String[] negatives = { "no", "never" };
    String[] family = { "mother", "mom", "father", "dad", "brother", "sister",
                 "aunt", "uncle", "grandpa", "grandma" };
    String[] pets = { "cat", "dog", "parrot", "lizard", "turtle", "gerbil",
                 "rabbit", "fish" };
    if (containsAnyOf(statement, negatives))
      response = "Why so negative?";

    else if (containsAnyOf(statement, family))
      response = "Tell me more about your family.";

    else if (containsAnyOf(statement, pets))
      response = "Tell me more about your pets.";

    else
      response = "Interesting. Tell me more.";

    return response;
  }

  public static void main(String args)
  {
    System.out.print("Hello. Let's talk.")

    if args
  }
}

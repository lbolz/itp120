class BiggestNum
{
    public static String findBiggest(String s1, String s2, String s3, String s4)
    {
        String[] s = s1 + s2 + s3 + s4;
        int[] toInt;
        int largest = 0;

        //convert all to ints
        for (String i : s)
            toInt += Integer.getInteger(s[i]);
        
        //find largest
        for (int i : toInt) 
        {
         if (toInt[i] > largest)
            largest = toInt[i]; 
        }

        return Integer.toString(largest);
    }

    public static void main(String[] args)
    {
        if (args.length == 4)
        {
            System.out.println(findBiggest(args[0], args[1], args[2], args[3]));
        }
        else
        {
            System.out.println("Please enter four command line arguments!");
        }
    }

}
class Test
{ 
    public static int arg[] = {3, 11, 7, 5};

    public static int largest()
        {
         int i;
         int max = arg[0];
         for (i = 1; i < arg.length; i++)
                if (arg[i] > max)
                        max = arg[i];
         return max;
        }
        public static void main(String[] args)
        {
         System.out.println("Largest in given array is " + largest());
        }
}
~                                                                                                                 
~    
public class Person 
{
    private double myHeight;
    private double myWeight;
    private String myHair;
    private String myName;

    public Person( String name, String weight, String height, String hair )
    {
      myHeight = Double.parseDouble(height);
      myWeight = Double.parseDouble(weight);
      myHair = hair;
      myName = name;
    }
    
    public String toString()
    {
      return myName;
    }

    public void sleep( int hours )
    {
      for ( int i = 0 ; i < hours ; i++ )
        System.out.println( "Sleeping ... " );
    }

    public void talk()
    {
      myWeight -= 0.01;
      System.out.println( "Talking ... " );
    }

    public String getHair()
    {  
      return myHair;
    }

    public String getWeight()
    {
      return Double.toString(myWeight);
    }

    public String getHeight()
    {
      return Double.toString(myHeight);
    }
    
    public void eat()
    {
    myWeight += 0.5;
    System.out.println( "Eating ... " );
    }

  public void walk( int meters )
    {
    myWeight -= 0.05 * meters / 1000;
    System.out.println( "Walking ... " );
    }
    
  public void feast()
    {
    for (int i = 0; i < 5; i++)
      {
      eat();
      talk();
      eat();
      }
    }
}


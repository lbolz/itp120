public class PersonTest
{
  public static void main(String[] args)
  {
    if (args.length == 4)
    {
      Person someone = new Person(args[0], args[1], args[2], args[3]);

      someone.talk();
      someone.eat();
      someone.walk(2500);
      someone.feast();
      someone.sleep( 3 );

      System.out.println( someone + " is " + someone.getHeight() + "m and " + someone.getWeight() + "kg. They also have " + someone.getHair() + " hair."); 
      System.out.println( someone  + " now weighs " + someone.getWeight() + "kg" );
    }

    else
    {
      System.out.println("Please enter 4 arguments!");
      System.out.println("These arguments are: name, weight, height, and hair color");
    }
  }
}

class XcubedPlus 
{
    public static String theanswer(String userx)
    {
        int x = Integer.parseInt(userx);
        int answer = x * x * x + 5;

        return Integer.toString(answer);
    }

    public static void main(String[] args)
    {
        if (args.length == 1)
        {
            System.out.println(theanswer(args[0]));
        }
        else if (args.length > 1) 
        {
            System.out.println("Please enter only one argument!");
        }
        else
        {
            System.out.println("Please enter an argument!");
        }
    }
}
